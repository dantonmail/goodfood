import { NavLink } from 'react-router-dom';
const Nav = ({
  active = '',
  setActive = () => {},
  openBurger = '',
  setopenBurger = () => {},
  lineActive = '',
  setLineActive = () => {},
}) => {
  const burgerHandler = () => {
    if (active) {
      setActive(false);
      setopenBurger(!openBurger);
      setLineActive(!lineActive);
    }

    return;
  };

  return (
    <nav className='header-topmenu__nav'>
      <ul
        className={
          active
            ? 'header-topmenu__list header-topmenu__list-active'
            : 'header-topmenu__list'
        }
      >
        <li className='header-topmenu__item'>
          <NavLink
            exact
            to='/'
            className='header-topmenu__link '
            activeClassName='header-topmenu__link__active'
            onClick={() => burgerHandler()}
          >
            Random dish
          </NavLink>
        </li>
        <li className='header-topmenu__item'>
          <NavLink
            exact
            to='/favorite'
            className='header-topmenu__link'
            activeClassName='header-topmenu__link__active'
            onClick={() => burgerHandler()}
          >
            Favorite
          </NavLink>
        </li>
        <li className='header-topmenu__item'>
          <NavLink
            exact
            to='/cart'
            className='header-topmenu__link'
            activeClassName='header-topmenu__link__active'
            onClick={() => burgerHandler()}
          >
            Cart
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
