import React from 'react';
import Itempage from '../Itempage';
import Header from '../Header';
import Favorites from '../Favorites';
import Cart from '../Cart';
import Modal from '../Modal';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Button from '../Button';
import Form from '../Form';

function App() {
  const favorite = useSelector((store) => {
    return store.favorite; //получение данных массива favorite из store
  });

  const cart = useSelector((store) => {
    return store.cart; //получение данных массива cart из store
  });

  const [thirdModal, setFmodal] = useState(false);

  const addModalHandler = () => {
    setFmodal(true);
  };

  const closeModal = () => {
    setFmodal(false);
  };

  return (
    <Router>
      <div className='App'>
        <div className='main__container'>
          <Header />
          <div className='cards'>
            <Switch>
              <Route exact path='/'>
                <Itempage />
              </Route>

              <Route exact path='/favorite'>
                <Favorites
                  data={favorite}
                  addFullStar={true}
                  addEmptyrtStar={false}
                />
                <div className='favorite__button'>
                  <Button
                    text='Add custom dish'
                    bgc='#8d81ac'
                    onClick={() => {
                      addModalHandler(); //обработка кнопки в Header
                    }}
                    btnClName='button'
                  />
                </div>
                <div className='favorite__modal'>
                  {thirdModal && (
                    <Modal
                      text=''
                      header='Add custom dish'
                      modalBg='green'
                      headerBg='darkgreen'
                      actions={<Form closeModalHandler={closeModal} />}
                      closeButton={true}
                      closeModalHandler={closeModal} //закрытие модалки
                    />
                  )}
                </div>
              </Route>
              <Route exact path='/cart'>
                <Cart data={cart} />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}
export default App;
