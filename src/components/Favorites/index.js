import React from 'react';
import Card from '../Card';

function Favorites({ data = [], addFullStar = '', addEmptyrtStar = '' }) {
  if (data === null || data.length === 0) {
    return <h1>No items to displey, please make your choice</h1>;
  }
  return data.map((data) => {
    return (
      <Card
        data={data}
        addButCart={true}
        delButCart={false}
        addFullStar={addFullStar}
        bgcolor='aliceblue'
        key={`${data.idMeal}`}
        addEmptyrtStar={addEmptyrtStar}
      />
    );
  });
}

export default Favorites;
