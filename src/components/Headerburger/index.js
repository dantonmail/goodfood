function Headerburger({
  active = '',
  setActive = () => {},
  openBurger = '',
  setopenBurger = () => {},
  lineActive = '',
  setLineActive = () => {},
}) {
  const burgerHandler = () => {
    setActive(!active);
    setopenBurger(!openBurger);
    setLineActive(!lineActive);
  };

  const burger = document.querySelector('.header-topmenu__burger');
  const topMenu = document.querySelector('.header-topmenu__list');
  const bl = document.querySelector('.header-topmenu__burger__line');

  const outsideEvtListener = (e) => {
    if (
      !active ||
      e.target === topMenu ||
      e.target === burger ||
      e.target === bl
    ) {
      return;
    }
    burgerHandler();
  };

  document.addEventListener('click', outsideEvtListener);

  return (
    <div className='header-topmenu__burger__wrapper'>
      <div
        className={
          openBurger
            ? 'header-topmenu__burger'
            : 'header-topmenu__burger header-topmenu__burger-active'
        }
        onClick={() => burgerHandler()}
      >
        <span
          className={
            !lineActive
              ? 'header-topmenu__burger__line'
              : 'header-topmenu__burger__line header-topmenu__burger__line-hide'
          }
        ></span>
      </div>
    </div>
  );
}

export default Headerburger;
