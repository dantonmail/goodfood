import '../Header/style.scss';
import Headerlogo from '../Headerlogo';
import Headernav from '../Headernav';
import Headerburger from '../Headerburger';
import { useState } from 'react';

function Header() {
  const [menuActive, setmenuActive] = useState(false);

  const [openBurger, setopenBurger] = useState(true); // открытие бургера
  const [lineActive, setLineActive] = useState(false); //линии бургера

  return (
    <header className='header'>
      <div className='container container__header'>
        <div className='header-topmenu'>
          <Headerlogo />
          <Headerburger
            active={menuActive}
            setActive={setmenuActive}
            openBurger={openBurger}
            setopenBurger={setopenBurger}
            lineActive={lineActive}
            setLineActive={setLineActive}
          />
          <Headernav
            active={menuActive}
            setActive={setmenuActive}
            openBurger={openBurger}
            setopenBurger={setopenBurger}
            lineActive={lineActive}
            setLineActive={setLineActive}
          />
        </div>
      </div>
    </header>
  );
}
export default Header;
