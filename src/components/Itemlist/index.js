import Card from '../Card';
import React from 'react';

function Itemlist({ data = [] }) {
  return (
    <>
      {data.map((data) => {
        return (
          <Card
            data={data}
            key={`${data.idMeal}`}
            skipBut='true'
            addEmptyrtStar='true'
            bgcolor='rgb(162, 189, 162)'
          />
        );
      })}
    </>
  );
}

export default Itemlist;
